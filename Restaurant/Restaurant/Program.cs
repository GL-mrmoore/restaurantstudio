﻿using System;
using System.Collections.Generic;


namespace Restaurant
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            MenuItem newItem1 = new MenuItem(4.99, "Garlic linguine", "Dinner", true);
            MenuItem newItem2 = new MenuItem(6.99, "Really good steak", "Dinner", false);
            MenuItem newItem3 = new MenuItem(2.99, "Delicious wings", "Appetizer", true);
            MenuItem newItem4 = new MenuItem(5.99, "Great dessert", "Dessert", true);

            DateTime today = DateTime.Now;
            List<MenuItem> startingMenu = new List<MenuItem>();
            // what happens if we add an item to the list of menu items before we create the menu that contains the list of menu items
            startingMenu.Add(newItem4);

            Menu ourMenu = new Menu(today, startingMenu);

            ourMenu.AddItem(newItem1);
            ourMenu.AddItem(newItem2);
            ourMenu.AddItem(newItem3);

            Console.WriteLine("=================");
            ourMenu.PrintMenu();
            Console.WriteLine("=================");

            ourMenu.PrintItem(newItem2);
            Console.WriteLine("=================");

            ourMenu.RemoveItem(newItem2);

            ourMenu.PrintMenu();















            Console.WriteLine("=================");
            //Console.WriteLine(newItem1.menuItemDate);
            //Console.WriteLine(newItem2.menuItemDate);
            //Console.WriteLine("1 equals 4" + newItem1.Equals(newItem4));

        }
    }
}
