﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant
{
    public class MenuItem
    {
        // fields
        public double Price { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public bool IsNew { get; set; }
        //public DateTime menuItemDate = new DateTime(2009, 12, 12);  // I might use a date here and a date in the Menu class to determine if it's new or not

        // constructor(s)
        public MenuItem(double price, string description, string category, bool isNew)
        {
            Price = price;
            Description = description;
            Category = category;
            IsNew = isNew;
        }
        public string IsItemNew()
        {
            if (IsNew)
            {
                return ("*");
            }
            else
            {
                return ("*");
            }
        }

        public override bool Equals(object comparison)
        {
            if (comparison == this)
            {
                return true;
            }

            if (comparison == null)
            {
                return false;
            }

            if (comparison.GetType() != GetType())
            {
                return false;
            }

            MenuItem theItem = comparison as MenuItem;
            return theItem.Description == Description;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Description);
        }





    }
}
