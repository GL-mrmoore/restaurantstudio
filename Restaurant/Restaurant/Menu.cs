﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant
{
    public class Menu
    {
        // fields
        public DateTime LastUpdated { get; }
        public List<MenuItem> Items { get; set; }

        // constructor(s)
        public Menu(DateTime dateUpdated, List<MenuItem> item)
        {
            LastUpdated = dateUpdated;
            Items = item;
        }

        public List<MenuItem> AddItem(MenuItem item)
        {
            Items.Add(item);
            return Items;
        }

        public List<MenuItem> RemoveItem(MenuItem item)
        {
            int toBeRemoved = -1;
            for (int i = 0; i < Items.Count; i++)
            {

                if (Items[i].Equals(item))
                {
                    toBeRemoved = i;
                }
            }

            Items.RemoveAt(toBeRemoved);
            return Items;
            // difference between using this method and just Remove()
        }

        public string MenuUpdated()
        {
            return ("The menu was last updated on " + LastUpdated);
        }

        // how i might decide if a menu item is new
        //public string isItemNew(someMenuItem)
        //{
        //    if ((menuDate - MenuItem.Date).TotalDays > 7) { then it is old }
        //}

        public void PrintItem(MenuItem item)
        {
            Console.WriteLine(item.Description);
            Console.WriteLine(item.Price);
        }

        public void PrintMenu()
        {
            for (int i = 0; i < Items.Count; i++)
            {
                Console.WriteLine(Items[i].Description + " " + Items[i].IsItemNew());
            }
        }











    }
}
